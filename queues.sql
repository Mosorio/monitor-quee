-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2020 at 01:09 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `queues`
--

-- --------------------------------------------------------

--
-- Table structure for table `queues`
--

CREATE TABLE `queues` (
  `Id` int(11) NOT NULL,
  `TestNumero` int(10) NOT NULL,
  `Peticion` int(10) NOT NULL,
  `Fecha` varchar(30) NOT NULL,
  `Time` varchar(50) NOT NULL,
  `Numero_Mensajes_Anterior` int(10) NOT NULL,
  `Numero_Mensajes_Actual` int(10) NOT NULL,
  `Diferencia` int(10) NOT NULL,
  `Queue` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `queues`
--

INSERT INTO `queues` (`Id`, `TestNumero`, `Peticion`, `Fecha`, `Time`, `Numero_Mensajes_Anterior`, `Numero_Mensajes_Actual`, `Diferencia`, `Queue`) VALUES
(1, 1, 1, '2020-02-29', '18:34:29.438099', 0, 2681156, 2681156, 'ks-usage-queue'),
(2, 1, 2, '2020-02-29', '18:34:35.193803', 2681156, 2681141, 15, 'ks-usage-queue'),
(3, 1, 3, '2020-02-29', '18:34:40.333969', 2681141, 2681085, 56, 'ks-usage-queue'),
(4, 1, 4, '2020-02-29', '18:34:45.493805', 2681085, 2681038, 47, 'ks-usage-queue'),
(5, 2, 1, '2020-02-29', '18:36:00.487874', 0, 2680728, 2680728, 'ks-usage-queue'),
(6, 2, 2, '2020-02-29', '18:37:00.411293', 2680728, 2680405, 323, 'ks-usage-queue'),
(7, 2, 3, '2020-02-29', '18:37:59.568582', 2680405, 2680085, 320, 'ks-usage-queue'),
(8, 2, 4, '2020-02-29', '18:38:58.717487', 2680085, 2679734, 351, 'ks-usage-queue'),
(9, 2, 5, '2020-02-29', '18:39:57.872184', 2679734, 2679352, 382, 'ks-usage-queue'),
(10, 2, 6, '2020-02-29', '18:40:57.026641', 2679352, 2679060, 292, 'ks-usage-queue'),
(11, 2, 7, '2020-02-29', '18:41:56.191501', 2679060, 2678732, 328, 'ks-usage-queue'),
(12, 2, 8, '2020-02-29', '18:42:55.373104', 2678732, 2678459, 273, 'ks-usage-queue'),
(13, 2, 9, '2020-02-29', '18:43:54.698055', 2678459, 2678294, 165, 'ks-usage-queue'),
(14, 2, 10, '2020-02-29', '18:44:53.849551', 2678294, 2677873, 421, 'ks-usage-queue'),
(15, 2, 11, '2020-02-29', '18:45:53.032062', 2677873, 2677725, 148, 'ks-usage-queue'),
(16, 2, 12, '2020-02-29', '18:46:52.193829', 2677725, 2677309, 416, 'ks-usage-queue'),
(17, 2, 13, '2020-02-29', '18:47:51.385353', 2677309, 2676934, 375, 'ks-usage-queue'),
(18, 2, 14, '2020-02-29', '18:48:50.572804', 2676934, 2676614, 320, 'ks-usage-queue'),
(19, 2, 15, '2020-02-29', '18:49:49.745263', 2676614, 2676461, 153, 'ks-usage-queue'),
(20, 2, 16, '2020-02-29', '18:50:48.920031', 2676461, 2675970, 491, 'ks-usage-queue'),
(21, 2, 17, '2020-02-29', '18:51:48.098896', 2675970, 2675814, 156, 'ks-usage-queue'),
(22, 2, 18, '2020-02-29', '18:52:47.272019', 2675814, 2675397, 417, 'ks-usage-queue'),
(23, 2, 19, '2020-02-29', '18:53:46.445054', 2675397, 2675105, 292, 'ks-usage-queue'),
(24, 2, 20, '2020-02-29', '18:54:45.617655', 2675105, 2674948, 157, 'ks-usage-queue'),
(25, 2, 21, '2020-02-29', '18:55:44.791631', 2674948, 2674433, 515, 'ks-usage-queue'),
(26, 3, 1, '2020-02-29', '18:56:29.127910', 0, 2674216, 2674216, 'ks-usage-queue'),
(27, 2, 22, '2020-02-29', '18:56:43.995487', 2674433, 2674150, 283, 'ks-usage-queue'),
(28, 3, 2, '2020-02-29', '18:57:28.865277', 2674216, 2673954, 262, 'ks-usage-queue'),
(29, 2, 23, '2020-02-29', '18:57:43.149598', 2674150, 2673814, 336, 'ks-usage-queue'),
(30, 3, 3, '2020-02-29', '18:58:28.047514', 2673954, 2673545, 409, 'ks-usage-queue'),
(31, 2, 24, '2020-02-29', '18:58:42.372181', 2673814, 2673361, 453, 'ks-usage-queue'),
(32, 3, 4, '2020-02-29', '18:59:27.176282', 2673545, 2672906, 639, 'ks-usage-queue'),
(33, 2, 25, '2020-02-29', '18:59:41.537642', 2673361, 2672775, 586, 'ks-usage-queue'),
(34, 3, 5, '2020-02-29', '19:00:26.323198', 2672906, 2672508, 398, 'ks-usage-queue'),
(35, 2, 26, '2020-02-29', '19:00:40.742110', 2672775, 2672321, 454, 'ks-usage-queue'),
(36, 3, 6, '2020-02-29', '19:01:25.628572', 2672508, 2671824, 684, 'ks-usage-queue'),
(37, 2, 27, '2020-02-29', '19:01:39.883774', 2672321, 2671596, 725, 'ks-usage-queue'),
(38, 3, 7, '2020-02-29', '19:02:24.827275', 2671824, 2671275, 549, 'ks-usage-queue'),
(39, 2, 28, '2020-02-29', '19:02:39.056158', 2671596, 2671170, 426, 'ks-usage-queue'),
(40, 3, 8, '2020-02-29', '19:03:23.990086', 2671275, 2670766, 509, 'ks-usage-queue'),
(41, 2, 29, '2020-02-29', '19:03:38.320989', 2671170, 2670566, 604, 'ks-usage-queue'),
(42, 3, 9, '2020-02-29', '19:04:23.163186', 2670766, 2670096, 670, 'ks-usage-queue'),
(43, 2, 30, '2020-02-29', '19:04:37.458936', 2670566, 2669918, 648, 'ks-usage-queue'),
(44, 3, 10, '2020-02-29', '19:05:22.310300', 2670096, 2669624, 472, 'ks-usage-queue'),
(45, 2, 31, '2020-02-29', '19:05:36.637695', 2669918, 2669466, 452, 'ks-usage-queue'),
(46, 3, 11, '2020-02-29', '19:06:21.474342', 2669624, 2668958, 666, 'ks-usage-queue'),
(47, 2, 32, '2020-02-29', '19:06:35.795561', 2669466, 2668782, 684, 'ks-usage-queue'),
(48, 3, 12, '2020-02-29', '19:07:20.654360', 2668958, 2668317, 641, 'ks-usage-queue'),
(49, 2, 33, '2020-02-29', '19:07:34.968205', 2668782, 2668225, 557, 'ks-usage-queue'),
(50, 3, 13, '2020-02-29', '19:08:19.808611', 2668317, 2667778, 539, 'ks-usage-queue'),
(51, 2, 34, '2020-02-29', '19:08:34.124313', 2668225, 2667678, 547, 'ks-usage-queue'),
(52, 3, 14, '2020-02-29', '19:09:18.964358', 2667778, 2667133, 645, 'ks-usage-queue'),
(53, 2, 35, '2020-02-29', '19:09:33.287603', 2667678, 2667041, 637, 'ks-usage-queue');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `queues`
--
ALTER TABLE `queues`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `queues`
--
ALTER TABLE `queues`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
